const AmazonCognitoIdentity = require('amazon-cognito-identity-js');
const pool_data = {
    UserPoolId: process.env.COGNITO_POOL_ID, // Your user pool id here
    ClientId: process.env.COGNITO_CLIENT_ID // Your client id here
};
const user_pool = new AmazonCognitoIdentity.CognitoUserPool(pool_data);


module.exports = {
    user_pool
}