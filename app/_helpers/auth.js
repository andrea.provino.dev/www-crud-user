const request = require('request');
const jwt = require('jsonwebtoken');
const jwkToPem = require('jwk-to-pem');
/**
 * Authentication Middleware
 * 
 * 
 */
const middleware = async (req, res, next) => {

    try {

        // retrieve the header
        const auth = req.headers.authorization
        if (!auth) {
            // missing auth
            return res.status(401).send({
                message: "Authentication Error",
                code: "MISSING_AUTH_HEADERS",
                detail: {
                    info: "Authentication is needed, but not provided"
                }
            })
        }
        validateToken(auth.replace('Bearer ', ''))
        next()
    } catch (e) {
        res.status(401).send({
            message: "Authentication Error",
            detail: e
        })
    }
}

/**
 * Validate Token
 * 
 * @param {AWS Cognito Web Token} token 
 */
const validateToken = (token) => {
    const pool_region = process.env.COGNITO_POOL_REGION
    const pool_id = process.env.COGNITO_POOL_ID

    request({
        url: `https://cognito-idp.${pool_region}.amazonaws.com/${pool_id}/.well-known/jwks.json`,
        json: true
    }, function (error, response, body) {
        try {
            if (!error && response.statusCode === 200) {
                pems = {};
                var keys = body['keys'];
                for (var i = 0; i < keys.length; i++) {
                    //Convert each key to PEM
                    var key_id = keys[i].kid;
                    var modulus = keys[i].n;
                    var exponent = keys[i].e;
                    var key_type = keys[i].kty;
                    var jwk = { kty: key_type, n: modulus, e: exponent };
                    var pem = jwkToPem(jwk);
                    pems[key_id] = pem;
                }
                //validate the token
                var decodedJwt = jwt.decode(token, { complete: true });
                if (!decodedJwt) {
                    throw ("Not a valid JWT token");
                }

                var kid = decodedJwt.header.kid;
                var pem = pems[kid];
                if (!pem) {
                    throw ('Invalid token');
                }

                jwt.verify(token, pem, function (err, payload) {
                    if (err) {
                        throw ("Invalid Token.");
                    } else {
                        return true
                    }
                });
            } else {
                throw ("Unable to download JWKs");
            }
        } catch (e) {
            throw (e)
        }
    });

}
module.exports = middleware