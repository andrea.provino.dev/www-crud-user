//import user model
const { User } = require('../models/user.model');
const jwt = require('jsonwebtoken')

global.fetch = require('node-fetch'); //emulate fetch requests used in cognito-identity.js 
const { user_pool } = require('../../_config/cognito.config')
var AmazonCognitoIdentity = require('amazon-cognito-identity-js');


const cognitoLogIn = (req, res) => {
    var authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails({
        Username: req.body.username,
        Password: req.body.password
    });
    var user_data = {
        Username: req.body.username,
        Pool: user_pool
    }
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(user_data);
    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: async function (result) {
            const accesstoken = result.getAccessToken().getJwtToken();
            res.status(200).send({ result });
        },
        onFailure: (function (err) {
            res.status(400).send({
                err
            })
        })
    })
}

const cognitoSignIn = (req, res) => {
    let attributeList = [];
    if (Object.keys(req.body).length === 0) {
        return res.status(400).send({
            message: "User content can not be empty",
            template: {
                body: {
                    password: "Example8",
                    username: "user@mail.com"
                }
            }
        });
    }
    if (!req.body.password) {
        return res.status(400).send({
            message: "Missing password"
        });
    }
    if (!req.body.username) {
        return res.status(400).send({
            message: "Missing username"
        });
    }
    var dataEmail = {
        Name: 'email',
        Value: req.body.username
    };
    var attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail);
    attributeList.push(attributeEmail);
    try {
        user_pool.signUp(req.body.username, req.body.password, attributeList, null, function (err, result) {
            if (err) {
                console.log(err)
                switch (err.code) {
                    case 'InvalidParameterException':
                        res.status(400).send({
                            error: err
                        })
                }
                res.status(400).send({
                    error: err
                })
                return;
            }
            //create user
            User.create(
                {
                    //proper set with model schema
                    _id: result.userSub,
                    email: result.user.username,
                    isActive: true,
                    code: ""
                }
            )
            const payload = {
                _id: result.userSub,
                details: result.codeDeliveryDetails
            }
            res.status(200).send(payload);
        });
    } catch (err) {
        console.log(err);
    }

}

const cognitoVerifyEmail = (req, res) => {

    if (Object.keys(req.body).length === 0) {
        return res.status(400).send({
            message: "Body content cannot be empty",
            template: {
                body: {
                    "username": "user@mail.com",
                    "verificationCode": "000006"
                }
            }
        })
    }
    if (!req.body.username) {
        return res.status(400).send({
            message: "username is missing"
        })
    }
    if (!req.body.verificationCode) {
        return res.status(400).send({
            message: "verificationCode is missing"
        })
    }

    var user_data = {
        Username: req.body.username,
        Pool: user_pool
    };

    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(user_data);
    try {
        cognitoUser.confirmRegistration(req.body.verificationCode, true, function (err, result) {
            if (err) {
                res.status(400).send({
                    error: err
                })
                return;
            }
            res.status(200).send({
                code: result,
                message: "Email has been succesfully verified!"
            })
        });
    } catch (err) {
        console.log(err);
        res.status(400).send({
            error: err
        })
    }

}

const cognitoChangePassword = (req, res) => {
    /*
      if(!req.body){
        return res.status(400).send({
            message: "Body content cannot be empty"
        })
    }
    if(!req.body.username){
        return res.status(400).send({
            message: "username is missing"
        })
    }
    if(!req.body.verificationCode){
        return res.status(400).send({
            message: "verificationCode is missing"
        })
    }
    */
    var user_data = {
        Username: req.body.username,
        Pool: user_pool
    };

    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(user_data);
    try {
        cognitoUser.changePassword(req.body.oldPassword, req.body.newPassword, function (err, result) {
            if (err) {
                res.status(400).send({
                    error: err
                })
                return;
            }
            res.status(200).send({
                code: result,
                message: "Password has been succesfully changed!"
            })
        });
    } catch (err) {
        console.log(err);
        res.status(400).send({
            error: err
        })
    }

}
const cognitoConfirmPassword = (req, res) => {
    if (!Object.keys(req.body).length) {
        return res.status(400).send({
            message: "Body content cannot be empty",
            template: {
                body: {
                    "email": "test@mail.com",
                    "newPassword": "Example8%",
                    "verificationCode": "000006"
                }
            }
        })
    }
    /** TODO add checks */
    if (!req.body.email) {
        return res.status(400).send({
            message: "Email is missing"
        })
    }
    var user_data = {
        Username: req.body.email,
        Pool: user_pool
    };
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(user_data);
    cognitoUser.confirmPassword(req.body.verificationCode, req.body.newPassword, {
        onSuccess() {
            res.status(200).send({
                message: 'Password has been correctly changed',
                status: "SUCCESS"
            })
        },
        onFailure(err) {
            res.status(500).send({
                message: 'An error occurred',
                status: "ERROR",
                raw: err
            })
        }
    });
}

const cognitoResetPassword = (req, res) => {
    if (!Object.keys(req.body).length) {
        return res.status(400).send({
            message: "Body content cannot be empty",
            template: {
                body: {
                    "email": "test@mail.com",
                }
            }
        })
    }
    if (!req.body.email) {
        return res.status(400).send({
            message: "Email is missing"
        })
    }
    var user_data = {
        Username: req.body.email,
        Pool: user_pool
    };
    var cognitoUser = new AmazonCognitoIdentity.CognitoUser(user_data);
    cognitoUser.forgotPassword({
        onSuccess: function (data) {
            // successfully initiated reset password request
            res.status(200).send({
                data
            })
        },
        onFailure: function (err) {
            res.status(400).send({
                err
            })
        },
    });
}


//Create and save a new user
const create = (req, res) => {
    //VALIDATE REQUEST
    if (Object.key(req.body).length === 0) {
        return res.status(400).send({
            message: "User content can not be empty",
            template: {
                body: {
                    "firstname": "bell",
                    "lastname": "yep",
                    "date_of_birth": "30/2/40",
                    "gender": "NA",
                    "phone_number": "0118975851",
                    "email": "test@bellazio.com",
                    "password": "Example8%"
                }
            }
        });
    }
    if (!req.body.password) {
        return res.status(400).send({
            message: "Missing password"
        });
    }
    if (!req.body.email) {
        return res.status(400).send({
            message: "Missing mail"
        });
    }
    var dataEmail = {
        Name: 'emailplus',
        Value: req.body.username
    };
    var attributeEmail = new AmazonCognitoIdentity.CognitoUserAttribute(dataEmail);
    attributeList.push(attributeEmail);
    try {
        user_pool.signUp(req.body.email, req.body.password, attributeList, null, function (err, result) {
            if (err) {
                switch (err.code) {
                    case 'InvalidParameterException':
                        res.status(400).send({
                            error: err
                        })
                }
                res.status(400).send({
                    error: err
                })
                return;
            }
            var cognitoUser = result.user;
            res.send(cognitoUser);
            console.log('user name is ' + cognitoUser.getUsername());
            // create new user
            User.create(
                {
                    //proper set with model schema
                    name: req.body.name,
                    surname: req.body.surname,
                    fullname: req.body.name + " " + req.body.surname,
                    phoneNumber: req.body.phoneNumber,
                    password: req.body.password,
                    gender: req.body.gender,
                    age: req.body.age,
                    email: req.body.email,
                    isActive: true,
                }
            )
                .then(data => {
                    res.send(data);
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating the User"
                    })
                })


        });
    } catch (err) {
        console.log(err);
    }

    //CREATE a new user

}
//retrive and return all users from DB 
const readAll = (req, res) => {
    User.find()
        .then(users => {
            res.send(users);
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving notes."
            });
        });
}
//retrive and return a single user by its calling
const read = (req, res) => {
    //TODO
}
//retrive and return a single user with userId
const readById = (req, res) => {
    User.findById(req.params.userId)
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "User not found with id: " + req.params.userId
                });
            }
            res.send(user);
        })
        .catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "User not found with id " + req.params.userId
                });
            }
            return res.status(500).send({
                message: "Error retrieving user with id " + req.params.userId
            });
        })
}
//update a single user with userId
const update = (req, res) => {
    // Validate Request
    if (!req.body) {
        return res.status(400).send({
            message: "User content can not be empty"
        });
    }
    let newUserData = req.body;
    // Find user and update it with the request body
    User.findByIdAndUpdate(req.params.userId, { $set: newUserData }, { new: true })
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "User not found with id " + req.params.userId
                });
            }
            res.send(user);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "User not found with id " + req.params.userId
                });
            }
            return res.status(500).send({
                message: "Error updating user with id " + req.params.userId
            });
        });
}
//delete a single user with userId
const Delete = (req, res) => {
    User.findByIdAndRemove(req.params.userId)
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "User not found with it: " + req.params.userId
                })
            }
            res.send({ message: "User succesfully deleted" });
        }).catch(err => {
            if (err.kind === 'ObjectId' || err.name === "NotFound") {
                return res.status(404).send({
                    message: "User nont found with id " + req.params.userId
                })
            }
            return res.status(500).send({
                message: "Could not delete user with id: " + req.params.userId
            });
        })
}
//retrive and change isActive status of a single user with userId
const softdelete = (req, res) => {
    // Find user and update isActive status
    User.findByIdAndUpdate(req.params.userId, { $set: { isActive: false } }, { new: true })
        .then(user => {
            if (!user) {
                return res.status(404).send({
                    message: "User not found with id " + req.params.userId
                });
            }
            res.send(user);
        }).catch(err => {
            if (err.kind === 'ObjectId') {
                return res.status(404).send({
                    message: "User not found with id " + req.params.userId
                });
            }
            return res.status(500).send({
                message: "Error updating user with id " + req.params.userId
            });
        });
}

module.exports = {
    /** cognito services **/
    cognitoLogIn,
    cognitoSignIn,
    cognitoVerifyEmail,
    cognitoChangePassword,
    cognitoResetPassword,
    cognitoConfirmPassword,

    /** custom services **/
    create,
    readAll,
    read,
    readById,
    update,
    Delete,
    softdelete
}