const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    _id: {
        type: String,
        isRequired: true
    },
    firstname: {
        type: String,
        default: null,
        lowercase: true
    },
    lastname: {
        type: String,
        default: null,
        lowercase: true
    },
    fullname: {
        type: String,
        default: null,
        lowercase: true
    },
    email: {
        type: String,
        defualt: null,
        required: true,
        trim: true
    },
    date_of_birth: {
        type: Date,
        default: null
    },
    gender: {
        type: String,
        default: "NA",
        enum: ["M", "F", "NA"]
    },
    phone_number: {
        type: String,
        default: null,
        trim: true
    },
    is_active: {
        type: Boolean,
        default: true
    },
    status: {
        type: String,
        default: "UNCONFIRMED",
        enum: ["UNCONFIRMED", "CONFIRMED"]
    }
}, {
    minimize: false,
})
/*
TODO add later:
    lastLogin: { type: Date, default: Date.now},
    lastLogOut: {type: Date}, //used to measure log time
*/
const User = mongoose.model('users', UserSchema);
module.exports = {
    User
}