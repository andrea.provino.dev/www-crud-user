module.exports = (app) => {
    const user = require('../controllers/user.controller.js');
    const auth = require('../_helpers/auth')
    //Create a new User
    app.post('/users', user.create);

    //Read all Users
    app.get('/users', auth, user.readAll);

    //Read User (retrive personal info)
    app.get('/users-me', user.read);

    //Read User by ID
    app.get('/users/:userId', user.readById);

    //Update User by ID
    app.patch('/users/:userId', user.update);

    //Delete User by ID
    app.delete('/users/:userId', user.Delete);

    //Soft Delete User by Id
    app.patch('/users/:userId/soft-delete', user.softdelete);

    //cognito sign-in
    app.post("/users/sign-up", user.cognitoSignIn)

    //cognito verify email
    app.post("/users/verify-email", user.cognitoVerifyEmail)

    //cognito change password
    app.post("/users/change-password", user.cognitoChangePassword)

    //cognito request to reset password
    app.post("/users/lost-password", user.cognitoResetPassword)

    //cognito reset password
    app.post("/users/reset-password", user.cognitoConfirmPassword)

    //cognito log in
    app.post("/users/log-in", user.cognitoLogIn)

}