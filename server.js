//auto indent shift + alt + F

const express = require('express');
const bodyParser = require('body-parser');

//access dotenv
require('dotenv').config()

//create express app
const app = express();

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))

// parse requests of content-type - application/json
app.use(bodyParser.json())

//configuring the database
const dbConfig = require('./_config/database.config.js');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

//Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true,
}).then(() => {
    console.log("OK  - Connected to the database");
}).catch(err => {
    console.log("ERR - Could not connect to the database. Exiting now...", err);
    process.exit();
})

//define a simple route
app.get('/', (req, res) => {
    res.json({ "message": "everything is working fine" });
})

//Require User Routes
require('./app/routes/user.routes.js')(app);

// listen for requests
app.listen(8000, () => {
    console.log("Server is listening on port 8000");
});